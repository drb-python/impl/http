.. _install:

Installation of http driver
====================================
Installing ``drb-driver-http`` with execute the following in a terminal:

.. code-block::

   pip install drb-driver-http
