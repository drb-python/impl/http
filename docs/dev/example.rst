.. _example:

Example
=======

Connect to your server
----------------------
.. literalinclude:: example/connect.py
    :language: python

Download a file
----------------
.. literalinclude:: example/download.py
    :language: python

Get the attributes of a file
-----------------------------
.. literalinclude:: example/attributes.py
    :language: python