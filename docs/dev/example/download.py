import io

from drb.drivers.http import DrbHttpNode


url = 'http://localhost:8080/tests/resources/test.txt'
node = DrbHttpNode(url)

# Download all the file
with node.get_impl(io.BytesIO) as stream:
    stream.read().decode()

# Download the five first byte
with node.get_impl(io.BytesIO) as stream:
    stream.read(5).decode()

# Download only part of a file
with node.get_impl(io.BytesIO, start=10, end=110) as stream:
    stream.read().decode()
