from requests.auth import HTTPBasicAuth
from drb.drivers.http import DrbHttpNode, HTTPOAuth2

svc_url = 'https://www.url.com/service'
token_svc = 'https://auth.url.com/token'
user = 'user'
password = 'pass'
service_id = 'service'
secret = 'secret'

# Connect with no credential
node = DrbHttpNode(svc_url)

# Connect with Basic Auth
auth = HTTPBasicAuth(user, password)
node = DrbHttpNode(svc_url, auth=auth)

# Connect with OAuth2
auth = HTTPOAuth2(username=user, password=password, token_url=token_svc,
                  client_id=service_id, client_secret=secret)

node = DrbHttpNode(svc_url, auth=auth)
