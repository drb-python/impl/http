===================
Data Request Broker
===================
---------------------------------
HTTP driver for DRB
---------------------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-http/month
    :target: https://pepy.tech/project/drb-driver-http
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-http.svg
    :target: https://pypi.org/project/drb-driver-http/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-http.svg
    :target: https://pypi.org/project/drb-driver-http/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-http.svg
    :target: https://pypi.org/project/drb-driver-http/
    :alt: Python Version Support Badge

-------------------

This python module includes data model implementation of HTTP protocol

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

